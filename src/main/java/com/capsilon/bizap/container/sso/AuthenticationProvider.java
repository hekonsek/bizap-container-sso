package com.capsilon.bizap.container.sso;

public interface AuthenticationProvider {

    void authenticate(LoginRequest loginRequest);

}
