package com.capsilon.bizap.container.sso;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class InMemoryAuthenticationProvider implements AuthenticationProvider {

    @Value("${sso.supported.username:henryk}")
    private String supportedUsername;

    @Value("${sso.supported.password:secretKonsek}")
    private String supportedPassword;


    @Override
    public void authenticate(LoginRequest loginRequest) {
        if(!supportedUsername.equals(loginRequest.getUsername()) || !supportedPassword.equals(loginRequest.getPassword())) {
            throw new RuntimeException("Invalid login attempt.");
        }
    }

}