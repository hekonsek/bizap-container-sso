package com.capsilon.bizap.container.sso;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AuthenticationManager {

    private static final Logger LOG = LoggerFactory.getLogger(AuthenticationManager.class);

    @Autowired
    private List<AuthenticationProvider> authenticationProviders;

    TokenResponse authenticate(LoginRequest loginRequest) {
        for(AuthenticationProvider provider : authenticationProviders) {
            try {
                provider.authenticate(loginRequest);
                Algorithm algorithm = Algorithm.HMAC256("secret");
                return new TokenResponse(JWT.create()
                        .withIssuer("com.capsilon.bizap.container.sso")
                        .sign(algorithm));
            } catch (Exception e) {
                LOG.debug("Authentication attempt failed:", e);
            }
        }
        throw new RuntimeException("Cannot authenticate.");
    }

}
