package com.capsilon.bizap.container.sso;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@Controller
@EnableSwagger2
public class BizapContainerSsoApplication {

    @Autowired
    private MeterRegistry registry;

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping(path = "/api/sso/authenticate", produces = "application/json", consumes = "application/json")
    @ResponseBody
    public TokenResponse ssoAuthenticate(@RequestBody LoginRequest loginRequest) {
        Counter.builder("sso_authenticate").tag("username", loginRequest.getUsername()).register(registry).increment();
        return authenticationManager.authenticate(loginRequest);
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.ant("/api/**"))
                .build();
    }

    public static void main(String[] args) {
        SpringApplication.run(BizapContainerSsoApplication.class, args);
    }

}