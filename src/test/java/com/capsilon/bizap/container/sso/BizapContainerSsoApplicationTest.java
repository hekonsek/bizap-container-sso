package com.capsilon.bizap.container.sso;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import io.micrometer.core.instrument.MeterRegistry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BizapContainerSsoApplicationTest {

	RestTemplate rest = new RestTemplate();

	@Autowired
	MeterRegistry registry;

	@LocalServerPort
    int port;

	LoginRequest loginRequest = new LoginRequest("henryk", "secretKonsek");

	// Tests

	@Test
	public void shouldReturnToken() {
		TokenResponse tokenResponse = rest.postForObject("http://localhost:" + port + "/api/sso/authenticate",
                loginRequest, TokenResponse.class);
        assertThat(tokenResponse.getToken()).isNotBlank();
	}

    @Test
    public void shouldReturnValidToken() {
        TokenResponse tokenResponse = rest.postForObject("http://localhost:" + port + "/api/sso/authenticate",
				loginRequest, TokenResponse.class);
        Algorithm algorithm = Algorithm.HMAC256("secret");
        JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("com.capsilon.bizap.container.sso")
                .build();
        verifier.verify(tokenResponse.getToken());
    }

	@Test
	public void shouldGenerateMetricOnAuthentication() {
		rest.postForObject("http://localhost:" + port + "/api/sso/authenticate", loginRequest, TokenResponse.class);
		assertThat(registry.find("sso_authenticate").counter().count()).isGreaterThan(0);
	}

}